#include "air_service/modules/perception-fusion/pipeline/tools/msf_config_manager.h"

#include "base/common/log.h"
#include "base/io/protobuf_util.h"
#include "base/io/yaml_util.h"
#include "gflags/gflags.h"

namespace airos {
namespace perception {
namespace msf {
namespace tools {

namespace bg = boost::geometry;
typedef bg::model::point<double, 2, bg::cs::cartesian> point_t;
typedef bg::model::polygon<point_t, false, false> polygon_t;

MsfConfigManager::MsfConfigManager() {
  LoadMsfConfig(FLAGS_msf_config_file);
  LoadFusionParams(FLAGS_fusion_params_file);
}

bool MsfConfigManager::LoadMsfConfig(const std::string& msf_config_path) {
  return airos::base::ParseProtobufFromFile(msf_config_path, &msf_config_);
}

bool MsfConfigManager::LoadFusionParams(const std::string& fusion_params_path) {
  return airos::base::ParseProtobufFromFile(fusion_params_path,
                                            &fusion_params_);
}

}  // namespace tools
}  // namespace msf
}  // namespace perception
}  // namespace airos
