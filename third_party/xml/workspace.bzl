def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "xml",
        build_file = clean_dep("//third_party/xml:xml.BUILD"),
        path = "/opt/xml"
    )