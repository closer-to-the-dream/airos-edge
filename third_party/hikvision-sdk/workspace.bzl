def clean_dep(dep):
    return str(Label(dep))

def repo():
    native.new_local_repository(
        name = "hiksdk",
        build_file = clean_dep("//third_party/hikvision-sdk:hiksdk.BUILD"),
        path = "/opt/hikvision-sdk"
    )