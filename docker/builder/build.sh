#!/usr/bin/env bash

set -e

CURR_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd -P)"

function build_docker {
    local dckfile=$1
    local dckname="${dckfile%.*}"
    echo "docker build --network=host --no-cache=true -t ${dckname} -f ${dckfile} ${CURR_DIR}"
    docker build --network=host --no-cache=true -t ${dckname} -f ${dckfile} ${CURR_DIR}
}

function main {
    local dckfile=$1
    build_docker ${dckfile}
}

main "$@"
