#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_tensorrt {
    local url="https://zhilu.bj.bcebos.com/tensorrt.tar.gz"
    local sha256="5fa980c6ace297aa89474a589e9fcc0e16027f736c090ccb81048626b242cf02"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "tensorrt.tar.gz" "${sha256}"; then
            tar -zxf tensorrt.tar.gz
        else
            echo "check_sha256 failed: tensorrt.tar.gz"
        fi
        rm tensorrt.tar.gz
    popd >/dev/null
}

function main {
    install_tensorrt
}

main "$@"
