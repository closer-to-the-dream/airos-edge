#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_paho {
    local url="https://zhilu.bj.bcebos.com/Eclipse-Paho-MQTT-C-1.3.11-Linux.tar.gz"
    local sha256="498d3739c88818582824d7b7fb7b4f78f60119fe228cb1357d7e72b09dda947a"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "Eclipse-Paho-MQTT-C-1.3.11-Linux.tar.gz" "${sha256}"; then
            tar -zxf Eclipse-Paho-MQTT-C-1.3.11-Linux.tar.gz
        else
            echo "check_sha256 failed: Eclipse-Paho-MQTT-C-1.3.11-Linux.tar.gz"
        fi
        mv Eclipse-Paho-MQTT-C-1.3.11-Linux paho
        rm Eclipse-Paho-MQTT-C-1.3.11-Linux.tar.gz
    popd >/dev/null
}

function main {
    install_paho
}

main "$@"