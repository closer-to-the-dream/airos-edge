#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_videodecoder {
    local url="https://zhilu.bj.bcebos.com/video_decoder.tar.gz"
    local sha256="68259de1045c82faac5662d4fe31cad7bf616ef292cb2e221a9a3b0b4f967b7b"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "video_decoder.tar.gz" "${sha256}"; then
            tar -zxf video_decoder.tar.gz
        else
            echo "check_sha256 failed: video_decoder.tar.gz"
        fi
        rm video_decoder.tar.gz
    popd >/dev/null
}

function main {
    install_videodecoder
}

main "$@"