#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_xml {
    local url="https://zhilu.bj.bcebos.com/xml.tar.gz"
    local sha256="39777618f35220b87a852e68d620a9acef0f4865da7d3afda404c574ac124864"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "xml.tar.gz" "${sha256}"; then
            tar -zxf xml.tar.gz
        else
            echo "check_sha256 failed: xml.tar.gz"
        fi
        rm xml.tar.gz
    popd >/dev/null
}

function main {
    install_xml
}

main "$@"