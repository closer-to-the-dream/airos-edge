#!/usr/bin/env bash

set -e

source /opt/installers/install_base.sh

function install_glew {
    local url="https://zhilu.bj.bcebos.com/glew.tar.gz"
    local sha256="3ff1fa73ef1e21850e243511214688e81a3d69f4512933b66927578bd9f12655"
    [ -z "${url}" ] && exit
    pushd /opt/ >/dev/null
        wget -c ${url}
        if check_sha256 "glew.tar.gz" "${sha256}"; then
            tar -zxf glew.tar.gz
        else
            echo "check_sha256 failed: glew.tar.gz"
        fi
        rm glew.tar.gz
    popd >/dev/null
}

function main {
    install_glew
}

main "$@"