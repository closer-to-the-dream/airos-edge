light_info_list {
  light_id: 1
  light_type: LEFT_DIRECTION
  light_status: RED
  count_down: 9
  step_info_list {
    light_status: RED
    duration: 40
  }
  step_info_list {
    light_status: GREEN
    duration: 57
  }
  step_info_list {
    light_status: YELLOW
    duration: 3
  }
  right_way_time: 60
}
light_info_list {
  light_id: 2
  light_type: STRAIGHT_DIRECTION
  light_status: RED
  count_down: 9
  step_info_list {
    light_status: RED
    duration: 20
  }
  step_info_list {
    light_status: GREEN
    duration: 27
  }
  step_info_list {
    light_status: YELLOW
    duration: 3
  }
  right_way_time: 50
}
light_info_list {
  light_id: 3
  light_type: LEFT_DIRECTION
  light_status: RED
  count_down: 9
  step_info_list {
    light_status: RED
    duration: 40
  }
  step_info_list {
    light_status: GREEN
    duration: 57
  }
  step_info_list {
    light_status: YELLOW
    duration: 3
  }
  right_way_time: 60
}
time_stamp: 1669033782095
period: 100
confidence: 1
data_source: SIGNAL
work_status: DEV_NORMAL
control_mode: LOCAL_FIX_CYCLE
period_count_down: 39
sequence_num: 128