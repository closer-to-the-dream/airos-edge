# proto-file: v2xpb-config-event-details.proto
# proto-message: Configure

event_detail{
  cloud_topic_suffix: "speedLimit"
  rte_detail{
    event_type_id: 85
    description: ""
    flag_two_points: true
  }
}
event_detail{
  perception_name: "REGIONAL_CONSTRUCTION"
  cloud_topic_suffix: "construction"
  rte_detail{
    event_type_id: 501
    description: "Construction on the lane"
    flag_two_points: true
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  cloud_topic_suffix: "specialLane"
  rte_detail{
    event_type_id: 123
    description: "Bus Lane"
    flag_two_points: true
  }
}
event_detail{
  cloud_topic_suffix: "tidalLine"
  rte_detail{
    event_type_id: 41
    description: "Tidal line"
    flag_two_points: true
  }
}
event_detail{
  perception_name: "LANE_CONGESTION"
  cloud_topic_suffix: "trafficJam"
  rte_detail{
    event_type_id: 707
    description: "Traffic Jam on the lane"
    flag_two_points: true
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  cloud_topic_suffix: "trafficControl"
  rte_detail{
    event_type_id: 46
    description: "Traffic Control"
    flag_two_points: true
  }
}
event_detail{
  perception_name: "ZOMBIES_CAR"
  cloud_topic_suffix: "illegalOccupation"
  rte_detail{
    event_type_id: 412
    description: "Danger, there's zombies vehicle on the lane"
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  perception_name: "EMERGENCY_BRAKING"
  cloud_topic_suffix: "emergencyBrake"
  rte_detail{
    event_type_id: 802
    description: "Danger, emergency braking happened on the lane"
    priority: 7
  }
}
event_detail{
  perception_name: "TRAFFIC_ACCIDENT"
  cloud_topic_suffix: "trafficAccident"
  rte_detail{
    event_type_id: 103
    description: "Traffic Accident happened on the lane"
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  cloud_topic_suffix: "emergencyLaneOccupy"
  rte_detail{
    event_type_id: 410
    description: "Danger, emergency lane is occupied on lane"
  }
}
event_detail{
  perception_name: "LOWSPEED"
  cloud_topic_suffix: "lowSpeed"
  rte_detail{
    event_type_id: 101
    description: "Low speed vehicle ahead"
    priority: 7
  }
}
event_detail{
  perception_name: "ANTIDROMIC"
  cloud_topic_suffix: "antiDromic"
  rte_detail{
    event_type_id: 904
    description: "Danger, there's antidromic vehicle on the lane"
    priority: 7
  }
}
event_detail{
  perception_name: "PEOPLE_ENTER"
  cloud_topic_suffix: "peopleEnter"
  rte_detail{
    event_type_id: 405
    description: "Pedestrian entering on the lane"
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  perception_name: "FRAGMENT"
  cloud_topic_suffix: "fragment"
  rte_detail{
    event_type_id: 401
    description: "Throwing object on the lane"
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  cloud_topic_suffix: "emergencyParkingBelt"
  rte_detail{
    event_type_id: 57
    description: "Emergency parking belt ahead"
  }
}
event_detail{
  cloud_topic_suffix: "bridge"
  rte_detail{
    event_type_id: 8
    description: "Bridge ahead"
  }
}
event_detail{
  cloud_topic_suffix: "tunnel"
  rte_detail{
    event_type_id: 21
    description: "Tunnel ahead"
  }
}
event_detail{
  cloud_topic_suffix: "curveSection"
  rte_detail{
    event_type_id: 56
    description: "Curve section"
  }
}
event_detail{
  cloud_topic_suffix: "emergencyParkingArea"
  rte_detail{
    event_type_id: 58
    description: "Emergency parking area"
  }
}
event_detail{
  cloud_topic_suffix: "guideArrowTips"
  rte_detail{
    event_type_id: 59
    description: "Guide Arrow Tips"
  }
}
event_detail{
  cloud_topic_suffix: "decelerationSection"
  rte_detail{
    event_type_id: 60
    description: "Deceleration Section"
  }
}
event_detail{
  cloud_topic_suffix: "accidentSection"
  rte_detail{
    event_type_id: 34
    description: "Accident prone"
  }
}
event_detail{
  cloud_topic_suffix: "impasse"
  rte_detail{
    event_type_id: 43
    description: "Impasse"
  }
}
event_detail{
  cloud_topic_suffix: "attentionConfluence"
  rte_detail{
    event_type_id: 61
    description: "Attention confluence"
  }
}
event_detail{
  cloud_topic_suffix: "forbidTurnAround"
  rte_detail{
    event_type_id: 62
    description: "No turn around"
  }
}
event_detail{
  cloud_topic_suffix: "illegalOccupationSign"
  rte_detail{
    event_type_id: 63
    description: "Illegal occupation sign"
  }
}
event_detail{
  cloud_topic_suffix: "serviceArea"
  rte_detail{
    event_type_id: 64
    description: "service area"
  }
}
event_detail{
  perception_name: "OVERSPEED"
  cloud_topic_suffix: "overSpeed"
  rte_detail{
    event_type_id: 901
    description: "Overspeed ahead"
    priority: 7
  }
}
event_detail{
  perception_name: "NONVEHICLE_ENTER"
  cloud_topic_suffix: "nonvehicleEnter"
  rte_detail{
    event_type_id: 411
    description: "Non-motor Vehicle Entering on the lane"
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  cloud_topic_suffix: "trafficLightFailure"
  rte_detail{
    event_type_id: 902
    description: "Warning! Traffic light failure"
  }
}
event_detail{
  perception_name: "RAMP_ENTER_AFFLUX_MASTER"
  rte_detail{
    event_type_id: 413
    description: "RAMP ENTER AFFLUX MASTER occurred"
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  perception_name: "RAMP_ENTER_AFFLUX_BRANCH"
  rte_detail{
    event_type_id: 414
    description: "RAMP ENTER AFFLUX BRANCH occurred"
    priority: 7
    flag_reduce_frequency: true
  }
}
event_detail{
  perception_name: "PEOPLE_RUN_RED_LIGHT"
  rte_detail{
    event_type_id: 415
    description: "People run red light"
    priority: 7
  }
}
event_detail{
  perception_name: "NONVEHICLE_RUN_RED_C"
  rte_detail{
    event_type_id: 416
    description: "Non-motor Vehicle run red light"
    priority: 7
  }
}
event_detail{
  perception_name: "VEHICLE_RUN_RED_C"
  rte_detail{
    event_type_id: 417
    description: "Motor Vehicle run red light"
    priority: 7
  }
}
