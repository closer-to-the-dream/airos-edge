spatFrame {
  message_count: 99
  dsecond: 20480
  intersections {
    node_region: 44
    node_id: 10
    moy: 467875
    dsecond: 20480
    phases {
      id: 6
      phase_state {
        color: COLOR_PROTECTED_GREEN
        timing_start: 0
        timing_end: 8
        timing_duration: 12
      }
      phase_state {
        color: COLOR_YELLOW
        timing_start: 8
        timing_end: 11
        timing_duration: 3
      }
      phase_state {
        color: COLOR_RED
        timing_start: 11
        timing_end: 26
        timing_duration: 15
      }
    }
    phases {
      id: 7
      phase_state {
        color: COLOR_RED
        timing_start: 0
        timing_end: 26
        timing_duration: 65
      }
      phase_state {
        color: COLOR_PROTECTED_GREEN
        timing_start: 26
        timing_end: 58
        timing_duration: 32
      }
      phase_state {
        color: COLOR_YELLOW
        timing_start: 58
        timing_end: 61
        timing_duration: 3
      }
    }
    phases {
      id: 16
      phase_state {
        color: COLOR_RED
        timing_start: 0
        timing_end: 11
        timing_duration: 15
      }
      phase_state {
        color: COLOR_PROTECTED_GREEN
        timing_start: 11
        timing_end: 23
        timing_duration: 12
      }
      phase_state {
        color: COLOR_YELLOW
        timing_start: 23
        timing_end: 26
        timing_duration: 3
      }
    }
    phases {
      id: 17
      phase_state {
        color: COLOR_RED
        timing_start: 0
        timing_end: 61
        timing_duration: 65
      }
      phase_state {
        color: COLOR_PROTECTED_GREEN
        timing_start: 61
        timing_end: 93
        timing_duration: 32
      }
      phase_state {
        color: COLOR_YELLOW
        timing_start: 93
        timing_end: 96
        timing_duration: 3
      }
    }
  }
}
